//  Author: George Strauch
//  serial number: 54
//  Due Date: February 20 2019
//  Programming Assignment  2
//  Spring 2019 - CS 3358 - 252
//  Instructor: Husain	Gholoom
//
//  program to encrypt and decrypt strings of text


#include <iostream>
#include <vector>
using namespace std;


// this just makes any given string to all upper case
// this is needed because the toUpper() function only works with chars
//
// param 1: string reference
void makeStringUpperCase (string &str)
{
    for (int index = 0; index < str.size(); ++index)
    {
        str[index] = (char)toupper(str[index]);
    }
}


// this will return a vector of the plaintext alphabet
// the vector will contain chars A-Z + 0-9
//
// returns vector<char> that contains chars A - Z + 0 - 9
vector<char> getPlainTextAlphabet ()
{
    // vector that will contain all the chars in alphabetic order
    vector <char> actualAlphabet;

    // appends capital numbers A-Z
    for (int chr = 65; chr < 91; ++chr)
    {
        actualAlphabet.push_back((char)chr);
    }
    // appends numbers 0-9
    for (int num = 48; num < 58; ++num)
    {
        actualAlphabet.push_back((char)num);
    }
    return actualAlphabet;
}


// returns an integer version of the key that was a string to verify it
// when getting the key from the user, it comes in as a string to easily validate it
// this function takes the string version of the key, verified to be a number,
// and turns it back into an integer.
//
// returns integer version of string key
int cvtStrKeyToIntKey (const string strKey)
{
    // case if the string key is one digit (1-9)
    if (strKey.size() == 1)
    {
        return (int)strKey[0] - 48;
    }
        // case it is a 2 digits (10 - 35)
    else
    {
        return ((int)strKey[0] -48) * 10 + ((int)strKey[1] - 48);
    }
}



// validates the key
//
// param 1: string key value
// returns true if valid key, else false
bool validateKey (string strKey)
{
    // if more that 2 digits, return false
    if (strKey.size() > 2)
    {
        return false;
    }

    // checks one digit number
    else if (strKey[0] > 57 || strKey[0] < 48)
    {
        return false;
    }

    // checks 2 digit number
    else if (strKey.size() == 2)
    {
        // if the size is 2, the first digit must be between 1 - 3
        if (strKey[0] > 51 || strKey[0] < 49) { return false; }
        if(strKey[1] > 57 || strKey[1] < 48) { return false; }
        // without this line it will validate keys 36-39 which shouldn't be the case
        if (cvtStrKeyToIntKey(strKey) > 35) { return false; }
    }

    // uif passed all tests, return true
    return true;
}


// helper function for validate input, iterates through char vec
// to validate if a given char is contained within a char vector
//
// param 1: the vector of the plaintext alphabet
// param 2: the char to check if the vec contains it
// returns true if the chrToCheck is contained within the vector, else false
bool vectorContains (const vector <char> ALPHABET, char chrToCheck)
{
    for (int index = 0; index < ALPHABET.size(); ++index)
    {
        if (ALPHABET[index] == chrToCheck) { return true; }
    }

    return false;
}


// this will validate the user input for the encrypt and decrypt functions
// first validate that it is not empty
// then make it uppercase and makes sure that it does not contain invalid characters
//
// param 1: the string to validate, pass by reference to force to uppercase
// returns true if valid string, else false
bool validateUserInput (string &toCheck)
{
    makeStringUpperCase(toCheck);

    // this vector will contain the plaintext alphabet
    const vector <char> ACTUAL_ALPHA = getPlainTextAlphabet();

    if (toCheck.empty())
    {
        return false;
    }

    else
    {
        for (int index = 0; index < toCheck.size(); ++index)
        {
            if (!vectorContains(ACTUAL_ALPHA, toCheck[index])) { return false; }
        }
    }

    // if it passed all the tests, it is a valid input,
    // it is also all caps now which is needed later
    return true;
}


// this function will get the index of a character within a given vector of characters
// is for encrypting and decrypting text
//
// param 1: the vector of the used to search for the char
// param 2: the char the function is looking for
// returns the index of the char
// retuning -1 will cause a SIGSEGV error but will never happen
// because we verified the user input
// will return the index of the char
int getCharIndex (const vector<char> chrVec, const char chr)
{
    for (int i = 0; i < chrVec.size(); ++i)
    {
        if(chrVec[i] == chr) { return i; }
    }

    // should never be reached because we verified the input string
    return -1;
}


// this will make a 2 dimensional char vector then return it
// the 2d vec will have 2 sub-vectors, the real alphabet, and one shifted by the key
// vec[1] will be the original alphabet shifted |key| units to the left
// eg. if the key is 1, the vector will go as
// vec[0] will be A-Z + 0-9
// vec[1] will be B-z + 0-9 + A
//
// param 1: key to shift the alphabet by
// returns the 2x36 vector
vector < vector <char> > get2dVector (int key)
{
    // this is the vector that will be initialized later and will be returning
    vector < vector <char> > returnVec;

    // set retvec[0] to be the real alphabet
    returnVec.push_back(getPlainTextAlphabet());

    // this is the vector that will hold the offset alphabet
    vector <char> tmp(36);

    // for every char in the alphabet, shift it by the key and add it to new alphabet
    for (int index = 0; index < 36; ++index)
    {
        tmp[index] = returnVec[0][(key+index) % 36];
    }
    returnVec.push_back(tmp);
    return returnVec;
}


// gets the decrypted string given encrypted string and key
// done by getting the indexes of chars of that key and getting
// the chars of those indexes on the plaintext vector
//
// param 1: the input string
// param 2: the key
// returns the decrypted string
string getDecryptedStringFromKey (const string INPUT_STRING, const int KEY)
{
    // 2d vector that contains the encrypted and plain text. [0] is plain, [1] is encrypted
    const vector <vector <char> > ALPHABET = get2dVector(KEY);

    // this is the indexes of the characters
    vector<int> indexes;

    string toReturn;

    // for every char in the encrypted input string, get the index
    // for it in the shifted vector and push it onto the indexes vec
    for (int chrIndex = 0; chrIndex <INPUT_STRING.size(); ++chrIndex) {
        indexes.push_back(getCharIndex(ALPHABET[1], INPUT_STRING[chrIndex]));
    }

    // for every int in the now populated indexes vector get the plaintext of it
    for (int index = 0; index < indexes.size(); ++index) {
        toReturn += ALPHABET[0][indexes[index]];
    }

    return toReturn;
}


// gets the encrypted string given plaintext string and key
// done by getting the indexes of chars of the plaintext and getting
// the chars of those indexes on the key vector
//
// param 1: the input string
// param 2: the key
// returns the encrypted string
string getEncryptedStringFromKey (const string INPUT_STRING, const int KEY)
{
    // 2d vector that contains the encrypted and plain text. [0] is plain, [1-35] are encrypted
    const vector <vector <char> > ALPHABET = get2dVector(KEY);

    // this is the indexes of the characters
    vector<int> indexes;

    string toReturn;

    // for every char in the plaintext input string, get the index
    // for it in the plaintext alphabet vector and push it onto the indexes vec
    for (int chrIndex = 0; chrIndex < INPUT_STRING.size(); ++chrIndex)
    {
        indexes.push_back(getCharIndex(ALPHABET[0], INPUT_STRING[chrIndex]));
    }

    // for every int in the now populated indexes vector get the encrypted char of it
    for (int index = 0; index < indexes.size(); ++index) {
        toReturn += ALPHABET[1][indexes[index]];
    }
    return toReturn;
}


// function that is called when the user wants to encrypt a string of data
// first get the user message to encrypt and validates it
// then gets the key from the user and validates it
// then calls function to get the encrypted version of the text and prints it
void encrypt ()
{
    // contains the user input, will be set later
    string userInput;

    // encryption key
    string key;

    cout << "Enter your message: \n";

    //  gets user input and returns if its invalid
    cin >> userInput;
    if (!validateUserInput(userInput))
    {
        cout << "Invalid input \n";
        return;
    }

    // gets the key (as a string) and validates it, returns if invalid
    cout << "Enter the key number:\n";
    cin >> key;
    if (!validateKey(key))
    {
        cout << "Invalid Key Number\n";
        return;
    }

    int intKey = cvtStrKeyToIntKey(key);

    cout << "Your translated text is: \n" <<
    getEncryptedStringFromKey(userInput, intKey) << '\n';
}


// function that is called when the user wants to decrypt a string of encrypted text
// first get the user message to decrypt and validates it
// then gets the key from the user and validates it
// then calls function to get the decrypted version of the text from the key and prints it
void decrypt ()
{
    // contains the user input, will be set later
    string userInput;

    // encryption key
    string key;

    cout << "Enter your message: \n";

    //  gets user input and returns if its invalid
    cin >> userInput;
    if (!validateUserInput(userInput))
    {
        cout << "Invalid input \n";
        return;
    }

    // gets the key (as a string) and validates it, returns if invalid
    cout << "Enter the key number:\n";
    cin >> key;
    if (!validateKey(key))
    {
        cout << "Invalid Key Number\n";
        return;
    }

    int intKey = cvtStrKeyToIntKey(key);

    cout << "Your translated text is: \n"
    <<  getDecryptedStringFromKey(userInput, intKey) << '\n';
}


// main function to the program
// loops through asking what the user wants to to
// exits the program when the user enters "exit"
int main ()
{
    // string that will hold what the user enters
    string userInput;

    cout << "Welcome to cryptography\n\n";

    while (true)
    {
        cout << "What would you like to do with a message? "
             << "(encrypt, decrypt, exit)\n"
             << "Enter your choice: ";


        cin >> userInput;

        makeStringUpperCase(userInput);

        //cannot use switch statement with string so must use if
        if (userInput == "EXIT") { break; }
        else if (userInput == "ENCRYPT") { encrypt(); }
        else if (userInput == "DECRYPT") { decrypt(); }
        else { cout << "Invalid message\n"; }

        cout << "\n\n";
    }
    cout << "\n\nXXXXXXX and YYYYYYY Security Systems" << '\n';

    return 0;
}